import React, { useState } from "react";
import _ from "lodash";

import SearchBar from "./components/SearchBar";
import FeedItem from "./components/FeedItem";
import { feedFetcher } from "./utils";

import "./App.css";

const debouncedFeedFetcher = _.debounce(feedFetcher, 500);

function App() {
  const [items, setItems] = useState([]);
  const [message, setMessage] = useState("");

  return (
    <div className="App">
      {/* Search Bar */}
      <SearchBar
        searchCallback={debouncedFeedFetcher}
        setItems={setItems}
        setMessage={setMessage}
      />

      {/* Show any info messages below */}
      {message && (
        <div className="message__container">
          <h5>{message}</h5>
        </div>
      )}

      {/* Feed Container */}
      <div className="items__container">
        {items.map((item, i) => {
          return (
            <FeedItem
              key={i}
              title={item.title}
              thumbLink={item.media.m}
              author={item.author}
              date={item.date_taken}
              tags={item.tags}
              link={item.link}
            />
          );
        })}
      </div>
    </div>
  );
}

export default App;
