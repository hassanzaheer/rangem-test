import React from "react";

import "./FeedItem.css";

function FeedItem({ title, thumbLink, author, date, tags, link }) {
  return (
    <div className="item__container">
      <img src={thumbLink} alt={title} />
      <p>
        <strong>Author:</strong> {author}
      </p>
      <p>
        <strong>Date Taken:</strong> {new Date(date).toDateString()}
      </p>
      <p>
        <strong>Tags:</strong> {tags}
      </p>
      <a href={link} target="_blank" rel="noopener noreferrer">
        More Details
      </a>
    </div>
  );
}

export default FeedItem;
