import React from "react";

import "./SearchBar.css";

function SearchBar({ searchCallback, setItems, setMessage }) {
  return (
    <div className="search__container">
      <input
        type="text"
        className="search__input"
        placeholder="Type your search here..."
        autoFocus
        onChange={e => searchCallback(e.target.value, setItems, setMessage)}
      />
    </div>
  );
}

export default SearchBar;
