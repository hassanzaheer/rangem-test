import { feedFetcher } from "./utils";

describe("utils", () => {
  describe("feedFetcher", () => {
    beforeEach(() => {
      global.fetch = jest.fn().mockImplementation(() =>
        Promise.resolve({
          json: jest.fn(() => ({ items: [{ author: "hassan" }] }))
        })
      );
    });

    it("calls setItems for returned data", async () => {
      const mockedSetItem = jest.fn();

      await feedFetcher("hello", mockedSetItem, () => {});

      expect(mockedSetItem).toHaveBeenCalledTimes(1);
      expect(mockedSetItem).toHaveBeenCalledWith([{ author: "hassan" }]);
    });

    it("calls setItems with empty array if value is empty", async () => {
      const mockedSetItem = jest.fn();
      const mockedSetMessage = jest.fn();

      await feedFetcher("", mockedSetItem, mockedSetMessage);

      expect(mockedSetItem).toHaveBeenCalledTimes(1);
      expect(mockedSetMessage).toHaveBeenCalledTimes(1);
      expect(mockedSetMessage).toHaveBeenCalledWith("");
      expect(mockedSetItem).toHaveBeenCalledWith([]);
    });
  });
});
