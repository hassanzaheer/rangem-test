export function feedFetcher(value, setItems, setMessage) {
  setMessage("");
  if (value === "") {
    setItems([]);
    return;
  }

  return fetch(
    `https://cors-anywhere.herokuapp.com/` +
      `https://www.flickr.com/services/feeds/photos_public.gne?tags=${value}&format=json&nojsoncallback=1`,
    {
      headers: {
        Origin: null
      }
    }
  )
    .then(response => {
      return response.json();
    })
    .then(data => {
      setItems(data.items);
      if (data.items.length === 0) {
        setMessage("No results found");
      }
    });
}
